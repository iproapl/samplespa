SampleSPA
===============
AngularJSベースのSPAサンプル


Requirements
===============
* node.js ... nodeそのものでなくnpmが必要なので。バージョンは v5.9.1 など。
* npm ... nodeモジュールをインストールするのに使う。バージョンは 3.8.5 など。
* git ... nodeモジュールの取得時に必要。Windowsの場合は Git for Windows を入れておく。バージョンは 2.8.0 など。
* python ... node-gypでNativeツールをビルドするのに使う。2.7.x系。
* Visual Studio ... node-gypで使う。Windowsの場合のみ。2015 Community Editionをカスタムインストールしておく。(参考参照)
* ※Visual Studioそのものでなく、これに含まれているWindows8.1 用の Windows SDK を必要としている。
* ※Windows10マシンでやった時の話。


事前準備
===============
コマンドラインで操作をする。

*1. gulp, bower, dtsm をグローバルインストールする*

※すでにグローバルインストールしていればスキップ

    $ npm install -g gulp bower dtsm

*2. gitの設定をcore.autocrlf=falseとする*

※改行コード変換はしない。ありのままを受け入れる。

確認。

    $ git config --global -l
    ...
    core.autocrlf=false
    ...

Windowsマシンではこんな感じになってたらOK。※OSXではそもそもこのエントリはないかも。

設定。

    $ git config --global core.autocrlf false


初期設定
===============
コマンドラインで操作をする。

*1. gitでリポジトリをcloneする*

    $ git clone <cloneするリポジトリのurl>

*2. NodeモジュールとBowerコンポーネントをインストールする*

clone直後だと、node_modulesとbower_componentsがないので。

    $ cd <cloneしたローカルリポジトリ>
    $ npm install
    $ bower install

*3. TypeScript用の型情報(d.ts)をインストールする*

    $ dtsm install

完了。


動かし方
===============

    $ gulp serve

ブラウザが起動する。
 
参考
===============
[Windowsでnpm installの赤いエラーに悩まされているアナタへ](http://overmorrow.hatenablog.com/entry/2015/11/27/235935)
