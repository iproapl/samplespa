'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');
var webpack = require('webpack-stream');

var $ = require('gulp-load-plugins')();


function webpackWrapper(watch, test, callback) {
  var webpackOptions = {
    resolve: {
      extensions: ['', '.webpack.js', '.web.js', '.js', '.ts'],
      moduleDirectories: ['node_modules', 'bower_components']
    },
    watch: watch,
    module: {
      preLoaders: [
        { test: /\.ts$/, exclude: /node_modules/, loader: 'tslint-loader'}
      ],
      loaders: [
        { test: /\.ts$/, exclude: /node_modules/, loaders: ['ng-annotate', 'awesome-typescript-loader']},
//        { test: /\.js$/, exclude: /node_modules/, loaders: ['ng-annotate']}
      ]
    },
//    entry: {
//      app: './src/app/app.ts' // .始まりでないとだめなので
//    },
    output: {
//      filename: '[name].bundle.js'
      filename: 'app.bundle.js'
    }
  };

  if(watch) {
    webpackOptions.devtool = 'inline-source-map';
  }

  var webpackChangeHandler = function(err, stats) {
    if(err) {
      conf.errorHandler('Webpack')(err);
    }
    $.util.log(stats.toString({
      colors: $.util.colors.supportsColor,
      chunks: false,
      hash: false,
      version: false
    }));
    browserSync.reload();
    if(watch) {
      watch = false;
      callback();
    }
  };

  // webpackOptions.entryを使う場合は、sourcesに設定してもうまくいかない...
  var sources = [ path.join(conf.paths.src, '/app/app.ts') ];
//  var sources = [];
  if (test) {
//    sources.push(path.join(conf.paths.src, '/app/**/*.spec.ts'));
//    sources.push(path.join(conf.paths.src, '/app/**/*.spec.js'));
//    sources.push(path.join(conf.paths.src, '/app/**/main.controller.spec.js'));
//    sources.push(path.join(conf.paths.src, '/app/**/main.controller.spec.ts'));
//    sources.push(path.join(conf.paths.src, '/app/hoge/hoge.spec.js'));
  }

  return gulp.src(sources)
    .pipe(webpack(webpackOptions, null, webpackChangeHandler))
    .pipe(gulp.dest(path.join(conf.paths.tmp, '/serve/app')));
}

gulp.task('scripts', function () {
  return webpackWrapper(false, false);
});

gulp.task('scripts:watch', ['scripts'], function (callback) {
  return webpackWrapper(true, false, callback);
});

gulp.task('scripts:test', function () {
  return webpackWrapper(false, true);
});

gulp.task('scripts:test-watch', ['scripts'], function (callback) {
  return webpackWrapper(true, true, callback);
});

// webpackを使うので不要
//gulp.task('scripts-reload', function() {
//  return buildScripts()
//    .pipe(browserSync.stream());
//});

//gulp.task('scripts', function() {
//  return buildScripts();
//});

//function buildScripts() {
//  return gulp.src(path.join(conf.paths.src, '/app/**/*.js'))
//    .pipe($.eslint())
//    .pipe($.eslint.format())
//    .pipe($.size())
//};
