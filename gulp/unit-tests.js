// ユニットテスト設定
'use strict';

// 利用モジュール
var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');
var karma = require('karma');

// テストターゲット
var pathTestTargetSrcJs = [
  //path.join(conf.paths.src, '/**/!(*.spec).js')
  path.join(conf.paths.tmp, '/serve/app/app.bundle.js')
];

function runTests (singleRun, done) {
  var reporters = ['progress'];
  var preprocessors = {};

  // プリプロセッサ、レポータの設定
  if (singleRun) {
    pathTestTargetSrcJs.forEach(function(path) {
      preprocessors[path] = ['coverage'];
    });
    reporters.push('coverage')
  }

  // __dirname は このファイルのあるディレクトリを示す.
  var localConfig = {
    configFile: path.join(__dirname, '/../karma.conf.js'),
    singleRun: singleRun,
    autoWatch: !singleRun,
    reporters: reporters,
    preprocessors: preprocessors
  };
  
  // karma実行
  var server = new karma.Server(localConfig, function(failCount) {
    done(failCount ? new Error("Failed " + failCount + " tests.") : null);
  })
  server.start();
}

// タスク定義
gulp.task('test', ['scripts:test'], function(done) {
  runTests(true, done);
});

// タスク定義
gulp.task('test:auto', ['scripts:test-watch'], function(done) {
  runTests(false, done);
});
