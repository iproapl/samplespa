/// <reference path="../../typings/bundle.d.ts" />
'use strict';
// ルーティングの設定
// @ngInject
export class RouterConfig {
  constructor($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
    $stateProvider
      .state('top', {
        url: '/top',
        templateUrl: 'app/top/top.html',
        controller: 'TopController',
        controllerAs: 'top'
      })
      .state('main', {
        url: '/main',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'app/about/about.html',
        controller: 'AboutController',
        controllerAs: 'main'
      })
      .state('sub1', {
        url: '/sub1',
        templateUrl: 'app/sub1/sub1.html',
        controller: 'Sub1Controller',
        controllerAs: 'sub1'
      })
      .state('sub2', {
        url: '/sub2',
        templateUrl: 'app/sub2/sub2.html',
        controller: 'Sub2Controller',
        controllerAs: 'sub2'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      .state('todo', {
        url: '/todo',
        templateUrl: 'app/todo/todo.html',
        controller: 'TodoController',
        controllerAs: 'todo'
      })
      .state('user', {
        url: '/user',
        templateUrl: 'app/user/user.html',
        controller: 'UserController',
        controllerAs: 'user'
      })
      .state('logout', {
        url: '/logout',
        templateUrl: 'app/login/logout.html'
    });
  // デフォルトのルーティング設定.（上記で指定していない場合の遷移先）
  $urlRouterProvider.otherwise('/top');
}
}
