/// <reference path="../../typings/bundle.d.ts" />
'use strict';
// インジェクション処理後の処理.
// @ngInject
export function runBlock($log: ng.ILogService) {
// すべてのモジュールのロードが完了したら実行される
  $log.debug('runBlock end');
}
