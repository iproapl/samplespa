/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
// @ngInject
export class Sub1Controller {
  public myData: any;
  public chartObject: any;
  constructor () {
    // $scopeに直接プロパティを設定せず、コントローラ自身に設置すべきとのこと。
    // こんな感じで怒られる。
    // error  You should not set properties on $scope in controllers. Use controllerAs syntax and add data to "this"
    // var vm = this;     
    // vm.myData = [
    this.myData = [
    {
        'firstName': 'Cox',
        'lastName': 'Carney',
        'company': 'Enormo',
        'employed': true
    },
    {
        'firstName': 'Lorraine',
        'lastName': 'Wise',
        'company': 'Comveyer',
        'employed': false
    },
    {
        'firstName': 'Nancy',
        'lastName': 'Waters',
        'company': 'Fuelton',
        'employed': false
    }
    ];

  // vm.chartObject = {
  this.chartObject = {
    'type': 'PieChart',
    'displayed': false,
    'data': {
      'cols': [
        {
          'id': 'month',
          'label': 'Month',
          'type': 'string',
          'p': {}
        },
        {
          'id': 'laptop-id',
          'label': 'Laptop',
          'type': 'number',
          'p': {}
        },
        {
          'id': 'desktop-id',
          'label': 'Desktop',
          'type': 'number',
          'p': {}
        },
        {
          'id': 'server-id',
          'label': 'Server',
          'type': 'number',
          'p': {}
        },
        {
          'id': 'cost-id',
          'label': 'Shipping',
          'type': 'number'
        }
      ],
      'rows': [
        {
          'c': [
            {
              'v': 'January'
            },
            {
              'v': 19,
              'f': '42 items'
            },
            {
              'v': 12,
              'f': 'Ony 12 items'
            },
            {
              'v': 7,
              'f': '7 servers'
            },
            {
              'v': 4
            }
          ]
        },
        {
          'c': [
            {
              'v': 'February'
            },
            {
              'v': 13
            },
            {
              'v': 1,
              'f': '1 unit (Out of stock this month)'
            },
            {
              'v': 12
            },
            {
              'v': 2
            }
          ]
        },
        {
          'c': [
            {
              'v': 'March'
            },
            {
              'v': 24
            },
            {
              'v': 5
            },
            {
              'v': 11
            },
            {
              'v': 6
            }
          ]
        }
      ]
    },
    'options': {
      'title': 'Sales per month',
      'isStacked': 'true',
      'fill': 20,
      'displayExactValues': true,
      'vAxis': {
        'title': 'Sales unit',
        'gridlines': {
          'count': 10
        }
      },
      'hAxis': {
        'title': 'Date'
      }
    },
    'formatters': {}
  };

  }
}
