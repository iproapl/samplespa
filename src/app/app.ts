// webpack の エントリーポイント

// typescript用の型情報
/// <reference path="../../typings/bundle.d.ts" />

// strictモードに。TypeScript→JavaScriptへのトランスパイルの際になくなってしまうらしいので明示する。
'use strict';

// ES6構文での記述
// importして参照するとwebpackでまとめてくれる.
// import * as angular from 'angular';
// import * as malarkey from 'malarkey';// d.tsがないので。
// import * as ngAnimate from 'angular-animate';
// import * as ngCookies from 'angular-cookies';
// import * as ngSanitize from 'angular-sanitize';
// import * as ngResource from 'angular-resource';
// import * as uiRouter from 'angular-ui-router';
// import * as moment from 'moment';
import { AppConfig } from './app.config';
import { RouterConfig } from './app.route';
import { runBlock } from './app.run';
import { GithubContributor } from './components/githubContributor/githubContributor.service';
import { NavbarDeirectiveDefinition } from './components/navbar/navbar.directive';
import { acmeMalarkey } from './components/malarkey/malarkey.directive';
import { WebDevTecService } from './components/webDevTec/webDevTec.service';
import { TopController } from './top/top.controller';
import { MainController } from './main/main.controller';
import { AboutController } from './about/about.controller';
import { Sub1Controller } from './sub1/sub1.controller';
import { Sub2Controller } from './sub2/sub2.controller';
import { LoginController } from './login/login.controller';
import { LoginHttpService } from './components/login/loginHttp.service';
import { TodoController } from './todo/todo.controller';
import { TodoListDirectiveDefinition } from './todo/todolist.directive';
import { TodoItemDirectiveDefinition } from './todo/todoitem.directive';
import { TodoFocusDirectiveDefinition } from './todo/todofocus.directive';
import { UserController } from './user/user.controller';
// TypeScript用型定義
declare var malarkey: any; // d.tsがないので。
declare var moment: moment.MomentStatic;
//
namespace myapp {
// Angularアプリケーション名
const appName = 'myapp';
// 依存Angularモジュール
const depsAngularModules = [
  'ngAnimate',
  'ngCookies',
  'ngSanitize',
  'ngMessages',
  'ngAria',
  'ngResource',
  'ui.router',
  'ui.bootstrap',
  'toastr',
  'ui.grid',
  'googlechart'
  ];
// Angularモジュール生成
const app = angular.module(appName, depsAngularModules);
// 定数の登録
app.constant('malarkey', malarkey);
app.constant('moment', moment);
// 設定値の登録
app.config(AppConfig);
app.config(RouterConfig); // 設置するのコンストラクタ.
// run処理登録？？？
app.run(runBlock);
// サービスの登録
app.service('webDevTec', WebDevTecService);
app.service('githubContributor', GithubContributor); // 設定するのはコンストラクタ。サービスはシングルトン！
app.service('loginHttp', LoginHttpService);
// コントローラの登録
app.controller('TopController', TopController);
app.controller('MainController', MainController);
app.controller('AboutController', AboutController);
app.controller('Sub1Controller', Sub1Controller);
app.controller('Sub2Controller', Sub2Controller);
app.controller('LoginController', LoginController);
app.controller('TodoController', TodoController); // 設定するのはコンストラクタ 
app.controller('UserController', UserController);
// ディレクティブの登録
app.directive('navbar', NavbarDeirectiveDefinition.ddo);
app.directive('acmeMalarkey', acmeMalarkey);
app.directive('todoList', TodoListDirectiveDefinition.ddo); // <todo-list/>を意味する。ディレクティブ名は小文字始まり。
app.directive('todoItem', TodoItemDirectiveDefinition.ddo); // 設定するのはDDO もしくはファクトリ
app.directive('todoFocus', TodoFocusDirectiveDefinition.ddo); // 設定するのはDDO もしくはファクトリ
} // module
