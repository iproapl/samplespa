/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
import { WebDevTecService, ITecThing } from '../components/webDevTec/webDevTec.service';

// @ngInject
export class MainController {
  public awesomeThings: ITecThing[];
  public webDevTec: WebDevTecService;
  public classAnimation: string;
  public creationDate: number;
  public toastr: any;

  constructor ($timeout: ng.ITimeoutService, webDevTec: WebDevTecService, toastr: any) {
    this.awesomeThings = new Array();
    this.webDevTec = webDevTec;
    this.classAnimation = '';
    this.creationDate = 1455238191047;
    this.toastr = toastr;
    this.activate($timeout);
  }

  activate($timeout: angular.ITimeoutService) {
    this.getWebDevTec();

    var self = this;

    $timeout(function() {
      self.classAnimation = 'rubberBand';
    }, 4000);
  }

  showToastr() {
    this.toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
    this.classAnimation = '';
  }

  getWebDevTec() {
    this.awesomeThings = this.webDevTec.tec;
  }
}
