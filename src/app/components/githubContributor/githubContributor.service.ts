/// <reference path="../../../../typings/bundle.d.ts" />
'use strict';
// @ngInject
export class GithubContributor {
  public apiHost: string = 'https://api.github.com/repos/Swiip/generator-gulp-angular';

  constructor(private $log: ng.ILogService, private $http: ng.IHttpService) {

  }

  getContributors(limit: number = 30): ng.IPromise<any[]> {
    return this.$http.get(this.apiHost + '/contributors?per_page=' + limit)
      .then((response: any): any => {
        return response.data;
      })
      .catch((error: any): any => {
        this.$log.error('XHR Failed for getContributors.\n', error.data);
      });
  }
}
