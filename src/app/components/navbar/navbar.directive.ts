/// <reference path="../../../../typings/bundle.d.ts" />
'use strict';
import { LoginHttpService } from '../login/loginHttp.service';
// NavbarDirectiveDefinition定義クラス.
export class NavbarDeirectiveDefinition {
  // ディレクティブ登録用
  static ddo(): ng.IDirective {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'app/components/navbar/navbar.html',
      controller: NavbarController,
      controllerAs: 'navbar',
      bindToController: true
    };
  }
}

// NavbarControllerクラス.
// @ngInject
export class NavbarController {
  headerDto: any; // TODO 型定義
  auth: boolean;
  nickname: string;

  // コンストラクタ.
  // <navbar/>タグを評価するタイミングで実行される.
  constructor(private $log: ng.ILogService, private loginHttp: LoginHttpService, private $state: ng.ui.IStateService, private $window: ng.IWindowService, private $uibModal: ng.ui.bootstrap.IModalService) {
    this.auth = loginHttp.headerDto.auth;
    this.nickname = loginHttp.headerDto.nickname;
    this.headerDto = loginHttp.headerDto;
    $log.debug('NavbarController...constructor.');
  }

  // 管理者権限を保有しているか.
  hasAdminRole(): boolean {
    return this.headerDto.admin;
  }

  openPasswordView() {
    var modalInstance = this.$uibModal.open({
     size: 'lg',
     backdrop: 'static', // ダイアログの背後をクリックしてもダイアログを閉じない
     templateUrl: 'app/components/navbar/password.modal.html',
     controller: PasswordChangeController,
     controllerAs: 'password'
    });
    modalInstance.result.then(
      () => {
        this.$log.debug('パスワード変更できました!');
      }
    );
  }

  logout() {
    var ret = this.$window.confirm('本当？');
    if ( ret === false ) {
      return;
    }
    this.loginHttp.logout()
    .finally(() => {
      this.$state.go('logout'); // logout画面に遷移
    });
  }

}

// @ngInject
class PasswordChangeController {
  constructor(private $log: ng.ILogService, private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, private $window: ng.IWindowService) {
    // do nothing.
  }
  ok() {
    var ret = this.$window.confirm('変更してもよい？');
    if ( ret === false ) {
      return;
    }
    this.$log.debug('ok.');
    this.$uibModalInstance.close();
  }
  dismiss() {
    this.$log.debug('cancel.');
    this.$uibModalInstance.dismiss();
  }
}
