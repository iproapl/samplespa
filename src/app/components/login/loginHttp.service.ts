/// <reference path="../../../../typings/bundle.d.ts" />
'use strict';
// LoginService.
// $httpサービスを利用しRESTのloginAPIを叩く.
// @ngInject
export class LoginHttpService {
  // ログイン結果オブジェクト格納用. ログイン済みの場合headerDto.auth=trueとなる.
  headerDto: any; // TODO 型定義！
  // コンストラクタ.
  constructor(private $http: ng.IHttpService, private $log: ng.ILogService) {
    this.reset();
  }
  // ログイン処理.
  login(loginDto: any): ng.IHttpPromise<any> {
    this.$log.debug('login...');
    this.$log.debug(loginDto);
    return this.$http.post('/api/login', loginDto)
    .success((data: any, status: any) => {
      this.headerDto = data.headerDto;
    })
    .error((data: any, status: any) => {
      this.reset();
    });
  }
  // ログアウト処理.
  logout(): ng.IPromise<any> {
    this.$log.debug('logout.');
    return this.$http.post('/api/logout', '')
    .finally(() => {
      this.reset();
    });
  }
  // リセット処理. headerDtoをリセットする.
  reset() {
    this.headerDto = {
      auth: false,
      nickname: '',
      role: '',
      admin: false
    };
  }

}
