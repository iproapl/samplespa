/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
// TodoListDirectiveDefinition定義.
export class TodoListDirectiveDefinition {
  // ディレクティブ登録用
  static ddo(): ng.IDirective {
    return {
      templateUrl: 'app/todo/todolist.html',
      restrict: 'E',
      controller: TodoListController,
      controllerAs: 'todoList',
      bindToController: true // オブジェクトも指定できるようだ。
    };
  }
}

// TodoListディレクティブ用コントローラー
// @ngInject
export class TodoListController {
    todoItems: TodoItem[];
    message: string;

    // コンストラクタ
    constructor(private $scope: ng.IScope, private $timeout: ng.ITimeoutService, private $log: ng.ILogService) {
        this.todoItems = [];
        this.$log.debug('TodoListController...constructor.');
    }

    // TodoItemを追加する.
    public addTodoItem(msg: string) {
      let index = this.todoItems.length;
      this.todoItems.push({
        id: index,
        message: msg,
        done: false,
        isEditMode: false
      });
      this.message = '';
      this.$log.debug('TodoListController.addTodoItem...');
      this.$log.debug(this.todoItems);
    }

    // TodoItemを削除する.
    public removeTodoItem(id: number) {
      var index = 0;
      for (var i = 0; i < this.todoItems.length; i++) {
        if (this.todoItems[i].id === id) {
          index = i;
          break;
        }
      }
      this.todoItems.splice(index, 1);
    }

    // 完了件数取得.
    public calcDone(): number {
      var count = 0;
      this.todoItems.forEach((todoItem: TodoItem) => {
        count += todoItem.done ? 1 : 0;
      });
      return count;
    }
}

// TodoItemクラス. TODO ここに定義したくない。。。todoitem側に定義したい。
class TodoItem {
    id: number;
    message: string;
    done: boolean;
    isEditMode: boolean;
}
