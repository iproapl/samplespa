/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
// TodoItemディレクティブ
import { TodoListController } from './todolist.directive'; // 型情報が欲しいので。

// TodoItemディレクティブ定義クラス.
export class TodoItemDirectiveDefinition {
  // ディレクティブ登録用
  static ddo(): ng.IDirective {
    return {
      restrict: 'E', // 要素
      require: '^todoList',
      templateUrl: 'app/todo/todoitem.html',
      controller: TodoItemController,
      controllerAs: 'todoItem',
      bindToController: true,
      scope: { // 複雑度合い軽減のため、スコオープを分離。
        todo: '=todo' // 指定したコントローラで参照できるようになる。
      },
      link: TodoItemDirectiveDefinition.postLink
    };
  }

  // リンク処理.
  static postLink(scope: any, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, todoListController: TodoListController) {
    // link内では複雑なことはしない。コントローラに移譲する。 TODO linkそのものをなくせないか？
    // TodoItem削除処理を追加.
    scope.removeTodoItem = (id: number) => {
      todoListController.removeTodoItem(id);
    };
  }
}

// TodoItemディレクティブ用コントローラー.
// @ngInject
export class TodoItemController {
  todo: any; // ディレクティブのスコープで指定した要素を参照できる.

  // コンストラクタ
  constructor(private $scope: ng.IScope, private $log: ng.ILogService, private $timeout: ng.ITimeoutService) {
    // do nothing.
    this.$log.debug('TodoItemController...constructor.');
  }

  // 編集モードの開始
  startEdit() {
    this.todo.isEditMode = true;
  }

  // 編集終了
  updateTodoItem($event: any) {
    if ($event.type === 'keyup') {
      if ($event.which !== 13) { // エンターキー
        return;
      }
    } else if ($event.type !== 'blur') {
      return;
    }
    this.todo.isEditMode = false;
    $event.stopPropagation();
  }

  // 編集キャンセル
  cancelEdit() {
    if (!this.todo.isEditMode) {
      return;
    }
    this.todo.isEditMode = false;
  }

  // フォーカス
  focus(element: ng.IAugmentedJQuery) {
    this.$log.debug('TodoItemController.focus...');
    this.$log.debug(element);
    // 
    this.$scope.$watch('todoItem.todo.isEditMode', (newVal: any) => {
      this.$timeout(() => {
        element[0].focus();
      }, 0, false);
    });
  }
}
