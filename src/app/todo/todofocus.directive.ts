/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
// TodoFocusディレクティブ
import { TodoItemController } from './todoitem.directive'; // 型情報が欲しいので。

// TodoFocusディレクティブ定義クラス.
export class TodoFocusDirectiveDefinition {
  // directive登録時に使用する. Directive Definition Objectを返す.
  static ddo(): ng.IDirective {
    return {
      restrict: 'A', // 属性
      require: '^todoItem',
      scope: {}, // スコープ分離
      link: TodoFocusDirectiveDefinition.postLink
    };
  }

  // リンク処理.
  static postLink(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, todoItemController: TodoItemController) {
    // TODO どうにかしたい。linkせず、自身のコントローラで処理できないか？
    todoItemController.focus(element);
 }
}
