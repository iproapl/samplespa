/// <reference path="../../../typings/bundle.d.ts" />
'use strict';

interface IAsset {
  id: string;
  title: string;
  beginDate: string;
  endDate: string;
  registerTs: Date;
  modifyTs: Date;
  deleteFlg: boolean;
  version: string;
}

interface IAssetResourceClass<T> extends ng.resource.IResourceClass<T> {
  search: () => ng.resource.IResourceArray<T>;
  searchP: (params: any) => ng.resource.IResourceArray<T>;
}

// @ngInject
export class Sub2Controller {
  asset: Object;
  query: Object;
  $asset: IAssetResourceClass<IAsset>;
  assets: ng.resource.IResourceArray<IAsset>;
  hoge: any;
  constructor(private $resource: ng.resource.IResourceService, private $http: ng.IHttpService, private $log: ng.ILogService) {
    // $scopeに直接プロパティを設定せず、コントローラ自身に設置すべきとのこと。
    // こんな感じで怒られる。
    // error  You should not set properties on $scope in controllers. Use controllerAs syntax and add data to "this"

    // resource定義
    this.$asset = <IAssetResourceClass<IAsset>> $resource<IAsset>(
      'api/asset/:id',
      { id: '@id' },
      { update: { method: 'PUT' },
         search: { method: 'GET', url: 'api/asset/search?z=' + new Date(), isArray: true, cache: false }, // .GETでの検索用
         searchP: { method: 'POST', url: 'api/asset/search', isArray: true, cache: false } // .POSTでの検索用
       }
    );

    // 初期設定
    this.query = {};
    this.assets = this.$asset.search();
  }

  onclick() {
      this.$http({
        method: 'GET',
        url: '/api/asset/search',
        params: { z: + new Date() } // キャッシュ防止...
      })
      .success((data: any) => {
        this.$log.debug(data);
        this.hoge = data;
      })
      .error(() => {
        this.hoge = '残念!!!';
      });
  }

  onSearch() {
      this.$log.debug('onSearch...');
      this.$log.debug(this.query);
      this.assets = this.$asset.searchP(this.query);
  }

  onInsert() {
      this.$log.debug('onInsert...');
      this.$log.debug(this.asset);
      this.$asset.save(
        this.asset,
        () => { this.assets = this.$asset.searchP(this.query); }
      );
  }

}
