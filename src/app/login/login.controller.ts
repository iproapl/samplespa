/// <reference path="../../../typings/bundle.d.ts" />
'use strict';
import { LoginHttpService } from '../components/login/loginHttp.service';
// @ngInject
export class LoginController {
  user: Object;
  failed: boolean;
  // 
  constructor(private loginHttp: LoginHttpService, private $log: ng.ILogService, private $state: ng.ui.IStateService) {
    // 初期設定
    this.reset();
    this.$log.debug('LoginController...constructor');
  }

  // ログイン
  login() {
      this.loginHttp.login(this.user)
      .success(() => {
        this.$state.go('top'); // top画面へ遷移.
        this.failed = false;
        this.$log.debug('login SUCCESS!');
      })
      .error((data: any, status: any) => {
        this.failed = true;
        this.$log.debug('login FAILURE!');
      });
    }

  // クリア
  reset() {
      this.$log.debug('reset...');
      this.user = {
        username: '',
        password: ''
      };
      this.failed = false;
  }

}
