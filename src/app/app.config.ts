/// <reference path="../../typings/bundle.d.ts" />
'use strict';
// アプリケーションの設定.
// @ngInject
export class AppConfig {
  constructor($logProvider: ng.ILogProvider, toastrConfig: any, $ariaProvider: any) {
    // enable log
    $logProvider.debugEnabled(true);
    // set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
    //
    $ariaProvider.config({
      tabindex: false,
      ariaDisabled: false
    });
  }
}
