// karuma用の設定.
'use strict';

// 利用モジュール
var path = require('path');
var conf = require('./gulp/conf');
var _ = require('lodash');
var wiredep = require('wiredep');
var gulpUtil = require('gulp-util');

// HTMLファイルのセット
var pathSrcHtml = [
  path.join(conf.paths.src, '/**/*.html')
];

// テストで使用するファイルをリストアップ.
function listFiles() {
  var wiredepOptions = _.extend({}, conf.wiredep, {
    dependencies: true,
    devDependencies: true
  });

　// bower_modulesの設定
  var patterns = wiredep(wiredepOptions).js
  // テスト対象の設定 Array.pushだとうまくいかない。。。
  .concat([
    path.join(conf.paths.tmp, '/serve/app/app.bundle.js'),
  ])
  // テストコードの設定
  .concat([
    path.join(conf.paths.src, '/app/**/*.spec.js'),
//    path.join(conf.paths.src, '/app/**/githubContributor.*.spec.js'),
//    path.join(conf.paths.src, '/app/**/malarkey.*.spec.js'),
//    path.join(conf.paths.src, '/app/**/navbar.*.spec.js'),
//    path.join(conf.paths.src, '/app/**/webDevTec.*.spjs'),
//    path.join(conf.paths.src, '/app/main/main.controller.spec.js'),
  ])
  // HTMLの設定（必要？？？）
  .concat(pathSrcHtml);

  var files = patterns.map(function(pattern) {
//    return {
//      pattern: pattern
//    };
    return pattern;
  });

//  files.push({
//    pattern: path.join(conf.paths.src, '/assets/**/*'),
//    included: false,
//    served: true,
//    watched: false
//  });

  return files;
}

// 
module.exports = function(config) {
  // デフォルト定義
  var configuration = {
    // テストで使用するファイルリスト
    files: listFiles(),
    // 
    singleRun: true,
    // 
    autoWatch: false,
    // 
    ngHtml2JsPreprocessor: {
      stripPrefix: conf.paths.src + '/',
      moduleName: 'myapp'
    },
    // 
    logLevel: 'WARN',
    // 
    frameworks: ['jasmine'],
//    frameworks: ['jasmine', 'angular-filesort'],

//    angularFilesort: {
//      whitelist: [path.join(conf.paths.src, '/**/!(*.html|*.spec|*.mock).js')]
//    },
    //
    browsers : ['PhantomJS'],
    //
    plugins : [
      'karma-phantomjs-launcher',
//      'karma-angular-filesort',
      'karma-coverage',
      'karma-jasmine',
      'karma-ng-html2js-preprocessor'
    ],
    //
    coverageReporter: {
      type : 'html',
      dir : path.join(conf.paths.dist, '/coverage/')
    },
    //
    reporters: ['progress'],
    // ?
//    proxies: {
//      '/assets/': path.join('/base/', conf.paths.src, '/assets/')
//    }
  };

  // This is the default preprocessors configuration for a usage with Karma cli
  // The coverage preprocessor is added in gulp/unit-test.js only for single tests
  // It was not possible to do it there because karma doesn't let us now if we are
  // running a single test or not
  // テスト前処理の設定
  configuration.preprocessors = {};
  pathSrcHtml.forEach(function(path) {
    configuration.preprocessors[path] = ['ng-html2js'];
  });

  // This block is needed to execute Chrome on Travis
  // If you ever plan to use Chrome and Travis, you can keep it
  // If not, you can safely remove it
  // https://github.com/karma-runner/karma/issues/1144#issuecomment-53633076
  if(configuration.browsers[0] === 'Chrome' && process.env.TRAVIS) {
    configuration.customLaunchers = {
      'chrome-travis-ci': {
        base: 'Chrome',
        flags: ['--no-sandbox']
      }
    };
    configuration.browsers = ['chrome-travis-ci'];
  }
  
  // log
//  gulpUtil.log('  karma.conf.js ... :'+config.preprocessors['.tmp/serve/app/app.bundle.js']);  

  // configにonfigurationの内容を設定(上書き)
  config.set(configuration);

  // log!
//  config.files.forEach(function(file){
//    gulpUtil.log('  karma.conf.js ... :'+file);
//  });
//  gulpUtil.log('  karma.conf.js ... :'+config.preprocessors['.tmp/serve/app/app.bundle.js']);  
};
